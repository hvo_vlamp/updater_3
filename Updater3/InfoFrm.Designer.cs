﻿namespace Updater3
{
    partial class InfoFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cmdGetInfos = new System.Windows.Forms.Button();
            this.cmdSetServoyPfad = new System.Windows.Forms.Button();
            this.cmdSetPgPfad = new System.Windows.Forms.Button();
            this.cmdSetImphvPfad = new System.Windows.Forms.Button();
            this.cmdSetServoyDienst = new System.Windows.Forms.Button();
            this.cmdSetPostgreDienst = new System.Windows.Forms.Button();
            this.lblServoyDienstStatus = new System.Windows.Forms.Label();
            this.lblPostgreDienstStatus = new System.Windows.Forms.Label();
            this.txtImphvPfad = new System.Windows.Forms.TextBox();
            this.txtPostgrePfad = new System.Windows.Forms.TextBox();
            this.txtServoyPfad = new System.Windows.Forms.TextBox();
            this.txtPgDienst = new System.Windows.Forms.TextBox();
            this.txtServoyDienst = new System.Windows.Forms.TextBox();
            this.txtPgVers = new System.Windows.Forms.TextBox();
            this.txtServoyVers = new System.Windows.Forms.TextBox();
            this.txtHvoVers = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.label1.Size = new System.Drawing.Size(163, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Systeminformation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(0, 34);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label2.Size = new System.Drawing.Size(133, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "HVO2go Version:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Gray;
            this.label3.Location = new System.Drawing.Point(0, 59);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label3.Size = new System.Drawing.Size(119, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Servoy Version:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Gray;
            this.label4.Location = new System.Drawing.Point(0, 84);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label4.Size = new System.Drawing.Size(134, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Postgres Version:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Gray;
            this.label5.Location = new System.Drawing.Point(0, 109);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.label5.Size = new System.Drawing.Size(111, 35);
            this.label5.TabIndex = 4;
            this.label5.Text = "Servoy Dienst:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Gray;
            this.label6.Location = new System.Drawing.Point(0, 144);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label6.Size = new System.Drawing.Size(126, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Postgres Dienst:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Gray;
            this.label7.Location = new System.Drawing.Point(0, 169);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(0, 15, 0, 0);
            this.label7.Size = new System.Drawing.Size(98, 35);
            this.label7.TabIndex = 6;
            this.label7.Text = "Servoy Pfad:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Gray;
            this.label8.Location = new System.Drawing.Point(0, 204);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label8.Size = new System.Drawing.Size(113, 25);
            this.label8.TabIndex = 7;
            this.label8.Text = "Postgres Pfad:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Gray;
            this.label9.Location = new System.Drawing.Point(0, 229);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.label9.Size = new System.Drawing.Size(106, 25);
            this.label9.TabIndex = 8;
            this.label9.Text = "IMP-HV Pfad:";
            // 
            // cmdGetInfos
            // 
            this.cmdGetInfos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdGetInfos.ForeColor = System.Drawing.Color.Gray;
            this.cmdGetInfos.Location = new System.Drawing.Point(258, 613);
            this.cmdGetInfos.Name = "cmdGetInfos";
            this.cmdGetInfos.Size = new System.Drawing.Size(157, 39);
            this.cmdGetInfos.TabIndex = 9;
            this.cmdGetInfos.Text = "Lade Informationen";
            this.cmdGetInfos.UseVisualStyleBackColor = true;
            this.cmdGetInfos.Click += new System.EventHandler(this.cmdGetInfos_Click);
            // 
            // cmdSetServoyPfad
            // 
            this.cmdSetServoyPfad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSetServoyPfad.ForeColor = System.Drawing.Color.Gray;
            this.cmdSetServoyPfad.Location = new System.Drawing.Point(540, 182);
            this.cmdSetServoyPfad.Name = "cmdSetServoyPfad";
            this.cmdSetServoyPfad.Size = new System.Drawing.Size(47, 22);
            this.cmdSetServoyPfad.TabIndex = 18;
            this.cmdSetServoyPfad.Text = "---";
            this.cmdSetServoyPfad.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdSetServoyPfad.UseVisualStyleBackColor = true;
            this.cmdSetServoyPfad.Click += new System.EventHandler(this.cmdSetServoyPfad_Click);
            // 
            // cmdSetPgPfad
            // 
            this.cmdSetPgPfad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSetPgPfad.ForeColor = System.Drawing.Color.Gray;
            this.cmdSetPgPfad.Location = new System.Drawing.Point(540, 210);
            this.cmdSetPgPfad.Name = "cmdSetPgPfad";
            this.cmdSetPgPfad.Size = new System.Drawing.Size(47, 22);
            this.cmdSetPgPfad.TabIndex = 19;
            this.cmdSetPgPfad.Text = "---";
            this.cmdSetPgPfad.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdSetPgPfad.UseVisualStyleBackColor = true;
            // 
            // cmdSetImphvPfad
            // 
            this.cmdSetImphvPfad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSetImphvPfad.ForeColor = System.Drawing.Color.Gray;
            this.cmdSetImphvPfad.Location = new System.Drawing.Point(540, 238);
            this.cmdSetImphvPfad.Name = "cmdSetImphvPfad";
            this.cmdSetImphvPfad.Size = new System.Drawing.Size(47, 22);
            this.cmdSetImphvPfad.TabIndex = 20;
            this.cmdSetImphvPfad.Text = "---";
            this.cmdSetImphvPfad.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdSetImphvPfad.UseVisualStyleBackColor = true;
            // 
            // cmdSetServoyDienst
            // 
            this.cmdSetServoyDienst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSetServoyDienst.ForeColor = System.Drawing.Color.Gray;
            this.cmdSetServoyDienst.Location = new System.Drawing.Point(357, 122);
            this.cmdSetServoyDienst.Name = "cmdSetServoyDienst";
            this.cmdSetServoyDienst.Size = new System.Drawing.Size(47, 22);
            this.cmdSetServoyDienst.TabIndex = 21;
            this.cmdSetServoyDienst.Text = "---";
            this.cmdSetServoyDienst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdSetServoyDienst.UseVisualStyleBackColor = true;
            this.cmdSetServoyDienst.Click += new System.EventHandler(this.cmdSetServoyDienst_Click);
            // 
            // cmdSetPostgreDienst
            // 
            this.cmdSetPostgreDienst.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSetPostgreDienst.ForeColor = System.Drawing.Color.Gray;
            this.cmdSetPostgreDienst.Location = new System.Drawing.Point(357, 150);
            this.cmdSetPostgreDienst.Name = "cmdSetPostgreDienst";
            this.cmdSetPostgreDienst.Size = new System.Drawing.Size(47, 22);
            this.cmdSetPostgreDienst.TabIndex = 22;
            this.cmdSetPostgreDienst.Text = "---";
            this.cmdSetPostgreDienst.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdSetPostgreDienst.UseVisualStyleBackColor = true;
            this.cmdSetPostgreDienst.Click += new System.EventHandler(this.cmdSetPostgreDienst_Click);
            // 
            // lblServoyDienstStatus
            // 
            this.lblServoyDienstStatus.AutoSize = true;
            this.lblServoyDienstStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblServoyDienstStatus.ForeColor = System.Drawing.Color.Gray;
            this.lblServoyDienstStatus.Location = new System.Drawing.Point(411, 124);
            this.lblServoyDienstStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblServoyDienstStatus.Name = "lblServoyDienstStatus";
            this.lblServoyDienstStatus.Size = new System.Drawing.Size(53, 20);
            this.lblServoyDienstStatus.TabIndex = 23;
            this.lblServoyDienstStatus.Text = "status";
            // 
            // lblPostgreDienstStatus
            // 
            this.lblPostgreDienstStatus.AutoSize = true;
            this.lblPostgreDienstStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPostgreDienstStatus.ForeColor = System.Drawing.Color.Gray;
            this.lblPostgreDienstStatus.Location = new System.Drawing.Point(411, 152);
            this.lblPostgreDienstStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPostgreDienstStatus.Name = "lblPostgreDienstStatus";
            this.lblPostgreDienstStatus.Size = new System.Drawing.Size(53, 20);
            this.lblPostgreDienstStatus.TabIndex = 24;
            this.lblPostgreDienstStatus.Text = "status";
            // 
            // txtImphvPfad
            // 
            this.txtImphvPfad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.txtImphvPfad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtImphvPfad.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Updater3.Properties.Settings.Default, "imphv_path", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtImphvPfad.Enabled = false;
            this.txtImphvPfad.ForeColor = System.Drawing.Color.White;
            this.txtImphvPfad.Location = new System.Drawing.Point(159, 238);
            this.txtImphvPfad.Name = "txtImphvPfad";
            this.txtImphvPfad.Size = new System.Drawing.Size(375, 22);
            this.txtImphvPfad.TabIndex = 17;
            this.txtImphvPfad.Text = global::Updater3.Properties.Settings.Default.imphv_path;
            // 
            // txtPostgrePfad
            // 
            this.txtPostgrePfad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.txtPostgrePfad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPostgrePfad.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Updater3.Properties.Settings.Default, "pg_path", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtPostgrePfad.Enabled = false;
            this.txtPostgrePfad.ForeColor = System.Drawing.Color.White;
            this.txtPostgrePfad.Location = new System.Drawing.Point(159, 210);
            this.txtPostgrePfad.Name = "txtPostgrePfad";
            this.txtPostgrePfad.Size = new System.Drawing.Size(375, 22);
            this.txtPostgrePfad.TabIndex = 16;
            this.txtPostgrePfad.Text = global::Updater3.Properties.Settings.Default.pg_path;
            // 
            // txtServoyPfad
            // 
            this.txtServoyPfad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.txtServoyPfad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServoyPfad.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Updater3.Properties.Settings.Default, "servoy_path", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtServoyPfad.Enabled = false;
            this.txtServoyPfad.ForeColor = System.Drawing.Color.White;
            this.txtServoyPfad.Location = new System.Drawing.Point(159, 182);
            this.txtServoyPfad.Name = "txtServoyPfad";
            this.txtServoyPfad.Size = new System.Drawing.Size(375, 22);
            this.txtServoyPfad.TabIndex = 15;
            this.txtServoyPfad.Text = global::Updater3.Properties.Settings.Default.servoy_path;
            // 
            // txtPgDienst
            // 
            this.txtPgDienst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.txtPgDienst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPgDienst.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Updater3.Properties.Settings.Default, "postgre_dienst", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtPgDienst.Enabled = false;
            this.txtPgDienst.ForeColor = System.Drawing.Color.White;
            this.txtPgDienst.Location = new System.Drawing.Point(159, 150);
            this.txtPgDienst.Name = "txtPgDienst";
            this.txtPgDienst.Size = new System.Drawing.Size(192, 22);
            this.txtPgDienst.TabIndex = 14;
            this.txtPgDienst.Text = global::Updater3.Properties.Settings.Default.postgre_dienst;
            // 
            // txtServoyDienst
            // 
            this.txtServoyDienst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.txtServoyDienst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServoyDienst.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Updater3.Properties.Settings.Default, "servoy_dienst", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtServoyDienst.Enabled = false;
            this.txtServoyDienst.ForeColor = System.Drawing.Color.White;
            this.txtServoyDienst.Location = new System.Drawing.Point(159, 122);
            this.txtServoyDienst.Name = "txtServoyDienst";
            this.txtServoyDienst.Size = new System.Drawing.Size(192, 22);
            this.txtServoyDienst.TabIndex = 13;
            this.txtServoyDienst.Text = global::Updater3.Properties.Settings.Default.servoy_dienst;
            // 
            // txtPgVers
            // 
            this.txtPgVers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.txtPgVers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPgVers.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Updater3.Properties.Settings.Default, "pg_version", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtPgVers.Enabled = false;
            this.txtPgVers.ForeColor = System.Drawing.Color.White;
            this.txtPgVers.Location = new System.Drawing.Point(159, 93);
            this.txtPgVers.Name = "txtPgVers";
            this.txtPgVers.Size = new System.Drawing.Size(80, 22);
            this.txtPgVers.TabIndex = 12;
            this.txtPgVers.Text = global::Updater3.Properties.Settings.Default.pg_version;
            // 
            // txtServoyVers
            // 
            this.txtServoyVers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.txtServoyVers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtServoyVers.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Updater3.Properties.Settings.Default, "servoy_version", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtServoyVers.Enabled = false;
            this.txtServoyVers.ForeColor = System.Drawing.Color.White;
            this.txtServoyVers.Location = new System.Drawing.Point(159, 65);
            this.txtServoyVers.Name = "txtServoyVers";
            this.txtServoyVers.Size = new System.Drawing.Size(80, 22);
            this.txtServoyVers.TabIndex = 11;
            this.txtServoyVers.Text = global::Updater3.Properties.Settings.Default.servoy_version;
            // 
            // txtHvoVers
            // 
            this.txtHvoVers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.txtHvoVers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHvoVers.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::Updater3.Properties.Settings.Default, "hvo_version", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.txtHvoVers.Enabled = false;
            this.txtHvoVers.ForeColor = System.Drawing.Color.White;
            this.txtHvoVers.Location = new System.Drawing.Point(159, 37);
            this.txtHvoVers.Name = "txtHvoVers";
            this.txtHvoVers.Size = new System.Drawing.Size(80, 22);
            this.txtHvoVers.TabIndex = 10;
            this.txtHvoVers.Text = global::Updater3.Properties.Settings.Default.hvo_version;
            // 
            // InfoFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.ClientSize = new System.Drawing.Size(684, 664);
            this.Controls.Add(this.lblPostgreDienstStatus);
            this.Controls.Add(this.lblServoyDienstStatus);
            this.Controls.Add(this.cmdSetPostgreDienst);
            this.Controls.Add(this.cmdSetServoyDienst);
            this.Controls.Add(this.cmdSetImphvPfad);
            this.Controls.Add(this.cmdSetPgPfad);
            this.Controls.Add(this.cmdSetServoyPfad);
            this.Controls.Add(this.txtImphvPfad);
            this.Controls.Add(this.txtPostgrePfad);
            this.Controls.Add(this.txtServoyPfad);
            this.Controls.Add(this.txtPgDienst);
            this.Controls.Add(this.txtServoyDienst);
            this.Controls.Add(this.txtPgVers);
            this.Controls.Add(this.txtServoyVers);
            this.Controls.Add(this.txtHvoVers);
            this.Controls.Add(this.cmdGetInfos);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "InfoFrm";
            this.Text = "InfoFrm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button cmdGetInfos;
        private System.Windows.Forms.TextBox txtHvoVers;
        private System.Windows.Forms.TextBox txtServoyVers;
        private System.Windows.Forms.TextBox txtPgVers;
        private System.Windows.Forms.TextBox txtServoyDienst;
        private System.Windows.Forms.TextBox txtPgDienst;
        private System.Windows.Forms.TextBox txtServoyPfad;
        private System.Windows.Forms.TextBox txtPostgrePfad;
        private System.Windows.Forms.TextBox txtImphvPfad;
        private System.Windows.Forms.Button cmdSetServoyPfad;
        private System.Windows.Forms.Button cmdSetPgPfad;
        private System.Windows.Forms.Button cmdSetImphvPfad;
        private System.Windows.Forms.Button cmdSetServoyDienst;
        private System.Windows.Forms.Button cmdSetPostgreDienst;
        private System.Windows.Forms.Label lblServoyDienstStatus;
        private System.Windows.Forms.Label lblPostgreDienstStatus;
    }
}