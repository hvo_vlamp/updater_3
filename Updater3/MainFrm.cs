﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Updater3
{
    public partial class MainFrm : Form
    {
        public MainFrm()
        {
            InitializeComponent();
            customizeDesign();
            // ---   Systeminfo Tab öffnen ---
            openChildForm(new InfoFrm());
        }
        #region design
        private void customizeDesign()
        {
            panel2goUpdate.Visible      = false;
            panelServoyUpdate.Visible   = false;
            panelPGUpdate.Visible       = false;
            panelServoyInstall.Visible  = false;

        }

        private void hideSubMenu()
        {
            if (panel2goUpdate.Visible == true)
                panel2goUpdate.Visible = false;
            if (panelServoyUpdate.Visible == true)
                panelServoyUpdate.Visible = false;
            if (panelPGUpdate.Visible == true)
                panelPGUpdate.Visible = false;
            if (panelServoyInstall.Visible == true)
                panelServoyInstall.Visible = false;
        }

        private void showSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                hideSubMenu();
                subMenu.Visible = true;
            }
            else
                subMenu.Visible = false;


        }
        #endregion

        #region 2goUpdate
        private void cmd2goUpdate_Click(object sender, EventArgs e)
        {
            showSubMenu(panel2goUpdate);
        }

        private void cmd2goInfo_Click(object sender, EventArgs e)
        {
            hideSubMenu();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            hideSubMenu();
        }
        #endregion

        #region ServoyUpdate
        private void cmdServoyUpdate_Click(object sender, EventArgs e)
        {
            showSubMenu(panelServoyUpdate);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            hideSubMenu();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            hideSubMenu();
        }
        #endregion

        #region PGUpdate
        private void cmdPGinstall_Click(object sender, EventArgs e)
        {
            showSubMenu(panelPGUpdate);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            hideSubMenu();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            hideSubMenu();
        }
        #endregion

        #region ServoyInstall
        private void cmdServoyInstall_Click(object sender, EventArgs e)
        {
            showSubMenu(panelServoyInstall);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            hideSubMenu();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            hideSubMenu();
        }
        #endregion

        #region childForm
        private Form activeForm = null;
        private void openChildForm(Form childform)
        {
            if (activeForm != null)
                activeForm.Close();
            activeForm = childform;
            childform.TopLevel = false;
            childform.FormBorderStyle = FormBorderStyle.None;
            childform.Dock = DockStyle.Fill;
            panelChildForm.Controls.Add(childform);
            panelChildForm.Tag = childform;
            childform.BringToFront();
            childform.Show();
        }




        #endregion

        #region MakeMovable
        
        private bool mouseDown;
        private Point lastLocation;

        private void UpdateMain_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void UpdateMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void UpdateMain_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }


        #endregion

        #region buttonClicks
        private void cmdClose_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Wollen Sie den Updater beenden?", "Beenden", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                Environment.Exit(0);
            }
        }

        

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            openChildForm(new InfoFrm());
        }

        #endregion
    }
}
