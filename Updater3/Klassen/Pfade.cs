﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Updater3
{
    class Pfade
    {
        private static string pfad;
        private static string pfadArt;

        public Pfade(string aPfadArt)
        {
            pfadArt = aPfadArt;
            pfad = Properties.Settings.Default.servoy_path;
        }

        public string Pfad
        {
            get { return pfad; }
            set {
                // checken ob dieser Pfad vorhanden ist und falls nötig speichern
                pfad = value;
                if(CheckPfad() == false)
                {
                    pfad = "N/A";
                    if (pfadArt == "sy")
                    {
                        Properties.Settings.Default.servoy_path = pfad;
                        Properties.Settings.Default.Save();
                    }
                }
                else
                {
                    if (pfadArt == "sy")
                    {
                        Properties.Settings.Default.servoy_path = pfad;
                        Properties.Settings.Default.Save();
                    }
                }

            }
        }



        private bool CheckPfad()
        {
            // Servoy Pfad checken
            if (pfadArt == "sy")
            {
                if (Directory.Exists(pfad))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }


    }
}
