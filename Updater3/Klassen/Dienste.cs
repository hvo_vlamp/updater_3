﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;


namespace Updater3
{
    class Dienste
    {
        private static string dienstName;
        private static string dienstArt;
        
        public Dienste(string aDienst)
        {
            if (aDienst == "pg")
            {
                dienstName = Properties.Settings.Default.postgre_dienst;
                dienstArt = "pg";
            }
            if (aDienst == "sy")
            {
                dienstName = Properties.Settings.Default.servoy_dienst;
                dienstArt = "sy";
            }
        }
        

        public string DienstName
        {
            get { return dienstName; }
            // --- Prüfen ob dieser Dienst Vorhanden Ist
            set
            {
                if(value.Length > 0)
                {
                    ServiceController sc = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == value);

                    if (sc==null)
                    {
                        // ---  Nicht vorhanden => "N/A" speichern
                        if (dienstArt == "pg")
                        {
                            Properties.Settings.Default.postgre_dienst = "N/A";
                        }
                        if (dienstArt == "sy")
                        {
                            Properties.Settings.Default.servoy_dienst = "N/A";
                        }
                        dienstName = value;
                        Properties.Settings.Default.Save();
                        
                    }
                    else
                    {
                        // --- Dienst gefunden => value speichern 
                        if (dienstArt == "pg")
                        {
                            Properties.Settings.Default.postgre_dienst = value;
                            
                        }
                        if (dienstArt == "sy")
                        {
                            Properties.Settings.Default.servoy_dienst = value;
                        }
                        dienstName = value;
                        Properties.Settings.Default.Save();
                        
                    }

                    
                }
            }
        }

        public string CheckDienstStatus()
        {
            string lcReturn = "gestoppt";
            ServiceController scTest = ServiceController.GetServices().FirstOrDefault(s => s.ServiceName == dienstName);
            if (scTest==null)
            {
                return "Dienst nicht gefunden";
            }

            ServiceController sc = new ServiceController(dienstName);    
            lcReturn = sc.Status.ToString();

            return lcReturn;
        }

    }
}
