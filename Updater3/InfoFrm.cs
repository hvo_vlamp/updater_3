﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using Ookii;

namespace Updater3
{
    public partial class InfoFrm : Form
    {
        public InfoFrm()
        {
            InitializeComponent();
            checkDienste();
        }


        private void cmdGetInfos_Click(object sender, EventArgs e)
        {
            checkDienste();
        }

        private void checkVersionen()
        {
            Vers hvoVersion = new Vers("2go");
        }


        private void checkDienste()
        {
            
            Dienste pgDienst = new Dienste("pg");
            lblPostgreDienstStatus.Text = pgDienst.CheckDienstStatus();

            Dienste syDienst = new Dienste("sy");
            lblServoyDienstStatus.Text = syDienst.CheckDienstStatus();
        }

        private void cmdSetServoyDienst_Click(object sender, EventArgs e)
        {
            Dienste syDienst = new Dienste("sy")
            {
                DienstName = Microsoft.VisualBasic.Interaction.InputBox("Dienstnamen füllen\nAuf Groß/Kleinschreibung achten!", "Servoy-Dienst", "Servoy Application Service")
            };
            Console.WriteLine(syDienst.CheckDienstStatus());
            lblServoyDienstStatus.Text  = syDienst.CheckDienstStatus();
            Application.DoEvents();
        }

        private void cmdSetPostgreDienst_Click(object sender, EventArgs e)
        {
            Dienste pgDienst = new Dienste("pg")
            {
                DienstName = Microsoft.VisualBasic.Interaction.InputBox("Dienstnamen füllen\nAuf Groß/Kleinschreibung achten!", "Postgre-Dienst", "postgresql-x64-12")
            };
            Console.WriteLine(pgDienst.CheckDienstStatus());
            lblPostgreDienstStatus.Text = pgDienst.CheckDienstStatus();
            Application.DoEvents();
        }

        private void cmdSetServoyPfad_Click(object sender, EventArgs e)
        {
            Pfade syPfad = new Pfade("sy");
            var dialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            if (dialog.ShowDialog().GetValueOrDefault())
            {
                syPfad.Pfad =   dialog.SelectedPath;
            }

                 
        }
    }
}
