﻿namespace Updater3
{
    partial class MainFrm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.cmdClose = new System.Windows.Forms.Button();
            this.panelServoyInstall = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.cmdServoyInstall = new System.Windows.Forms.Button();
            this.panelPGUpdate = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.cmdPGinstall = new System.Windows.Forms.Button();
            this.panelServoyUpdate = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.cmdServoyUpdate = new System.Windows.Forms.Button();
            this.panel2goUpdate = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.cmd2goInfo = new System.Windows.Forms.Button();
            this.cmd2goUpdate = new System.Windows.Forms.Button();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelChildForm = new System.Windows.Forms.Panel();
            this.panelSideMenu.SuspendLayout();
            this.panelServoyInstall.SuspendLayout();
            this.panelPGUpdate.SuspendLayout();
            this.panelServoyUpdate.SuspendLayout();
            this.panel2goUpdate.SuspendLayout();
            this.panelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panelSideMenu.Controls.Add(this.cmdClose);
            this.panelSideMenu.Controls.Add(this.panelServoyInstall);
            this.panelSideMenu.Controls.Add(this.cmdServoyInstall);
            this.panelSideMenu.Controls.Add(this.panelPGUpdate);
            this.panelSideMenu.Controls.Add(this.cmdPGinstall);
            this.panelSideMenu.Controls.Add(this.panelServoyUpdate);
            this.panelSideMenu.Controls.Add(this.cmdServoyUpdate);
            this.panelSideMenu.Controls.Add(this.panel2goUpdate);
            this.panelSideMenu.Controls.Add(this.cmd2goUpdate);
            this.panelSideMenu.Controls.Add(this.panelLogo);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(250, 664);
            this.panelSideMenu.TabIndex = 0;
            // 
            // cmdClose
            // 
            this.cmdClose.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdClose.FlatAppearance.BorderSize = 0;
            this.cmdClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClose.ForeColor = System.Drawing.Color.Gainsboro;
            this.cmdClose.Location = new System.Drawing.Point(0, 612);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.cmdClose.Size = new System.Drawing.Size(250, 45);
            this.cmdClose.TabIndex = 8;
            this.cmdClose.Text = "Beenden";
            this.cmdClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClose.UseVisualStyleBackColor = true;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // panelServoyInstall
            // 
            this.panelServoyInstall.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelServoyInstall.Controls.Add(this.button7);
            this.panelServoyInstall.Controls.Add(this.button8);
            this.panelServoyInstall.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelServoyInstall.Location = new System.Drawing.Point(0, 529);
            this.panelServoyInstall.Name = "panelServoyInstall";
            this.panelServoyInstall.Size = new System.Drawing.Size(250, 83);
            this.panelServoyInstall.TabIndex = 7;
            // 
            // button7
            // 
            this.button7.Dock = System.Windows.Forms.DockStyle.Top;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.Color.Gray;
            this.button7.Location = new System.Drawing.Point(0, 40);
            this.button7.Name = "button7";
            this.button7.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button7.Size = new System.Drawing.Size(250, 40);
            this.button7.TabIndex = 1;
            this.button7.Text = "Einstellungen";
            this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Dock = System.Windows.Forms.DockStyle.Top;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.ForeColor = System.Drawing.Color.Gray;
            this.button8.Location = new System.Drawing.Point(0, 0);
            this.button8.Name = "button8";
            this.button8.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button8.Size = new System.Drawing.Size(250, 40);
            this.button8.TabIndex = 0;
            this.button8.Text = "Information";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // cmdServoyInstall
            // 
            this.cmdServoyInstall.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdServoyInstall.FlatAppearance.BorderSize = 0;
            this.cmdServoyInstall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdServoyInstall.ForeColor = System.Drawing.Color.Gainsboro;
            this.cmdServoyInstall.Location = new System.Drawing.Point(0, 484);
            this.cmdServoyInstall.Name = "cmdServoyInstall";
            this.cmdServoyInstall.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.cmdServoyInstall.Size = new System.Drawing.Size(250, 45);
            this.cmdServoyInstall.TabIndex = 6;
            this.cmdServoyInstall.Text = "Servoy Installation";
            this.cmdServoyInstall.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdServoyInstall.UseVisualStyleBackColor = true;
            this.cmdServoyInstall.Click += new System.EventHandler(this.cmdServoyInstall_Click);
            // 
            // panelPGUpdate
            // 
            this.panelPGUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelPGUpdate.Controls.Add(this.button4);
            this.panelPGUpdate.Controls.Add(this.button5);
            this.panelPGUpdate.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelPGUpdate.Location = new System.Drawing.Point(0, 401);
            this.panelPGUpdate.Name = "panelPGUpdate";
            this.panelPGUpdate.Size = new System.Drawing.Size(250, 83);
            this.panelPGUpdate.TabIndex = 5;
            // 
            // button4
            // 
            this.button4.Dock = System.Windows.Forms.DockStyle.Top;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.Gray;
            this.button4.Location = new System.Drawing.Point(0, 40);
            this.button4.Name = "button4";
            this.button4.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button4.Size = new System.Drawing.Size(250, 40);
            this.button4.TabIndex = 1;
            this.button4.Text = "Einstellungen";
            this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.Gray;
            this.button5.Location = new System.Drawing.Point(0, 0);
            this.button5.Name = "button5";
            this.button5.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button5.Size = new System.Drawing.Size(250, 40);
            this.button5.TabIndex = 0;
            this.button5.Text = "Information";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // cmdPGinstall
            // 
            this.cmdPGinstall.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdPGinstall.FlatAppearance.BorderSize = 0;
            this.cmdPGinstall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPGinstall.ForeColor = System.Drawing.Color.Gainsboro;
            this.cmdPGinstall.Location = new System.Drawing.Point(0, 356);
            this.cmdPGinstall.Name = "cmdPGinstall";
            this.cmdPGinstall.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.cmdPGinstall.Size = new System.Drawing.Size(250, 45);
            this.cmdPGinstall.TabIndex = 4;
            this.cmdPGinstall.Text = "Postgres Update";
            this.cmdPGinstall.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdPGinstall.UseVisualStyleBackColor = true;
            this.cmdPGinstall.Click += new System.EventHandler(this.cmdPGinstall_Click);
            // 
            // panelServoyUpdate
            // 
            this.panelServoyUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelServoyUpdate.Controls.Add(this.button2);
            this.panelServoyUpdate.Controls.Add(this.button3);
            this.panelServoyUpdate.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelServoyUpdate.Location = new System.Drawing.Point(0, 273);
            this.panelServoyUpdate.Name = "panelServoyUpdate";
            this.panelServoyUpdate.Size = new System.Drawing.Size(250, 83);
            this.panelServoyUpdate.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Top;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.Gray;
            this.button2.Location = new System.Drawing.Point(0, 40);
            this.button2.Name = "button2";
            this.button2.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button2.Size = new System.Drawing.Size(250, 40);
            this.button2.TabIndex = 1;
            this.button2.Text = "Einstellungen";
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Top;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.Gray;
            this.button3.Location = new System.Drawing.Point(0, 0);
            this.button3.Name = "button3";
            this.button3.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button3.Size = new System.Drawing.Size(250, 40);
            this.button3.TabIndex = 0;
            this.button3.Text = "Information";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // cmdServoyUpdate
            // 
            this.cmdServoyUpdate.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdServoyUpdate.FlatAppearance.BorderSize = 0;
            this.cmdServoyUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdServoyUpdate.ForeColor = System.Drawing.Color.Gainsboro;
            this.cmdServoyUpdate.Location = new System.Drawing.Point(0, 228);
            this.cmdServoyUpdate.Name = "cmdServoyUpdate";
            this.cmdServoyUpdate.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.cmdServoyUpdate.Size = new System.Drawing.Size(250, 45);
            this.cmdServoyUpdate.TabIndex = 2;
            this.cmdServoyUpdate.Text = "Servoy Update";
            this.cmdServoyUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdServoyUpdate.UseVisualStyleBackColor = true;
            this.cmdServoyUpdate.Click += new System.EventHandler(this.cmdServoyUpdate_Click);
            // 
            // panel2goUpdate
            // 
            this.panel2goUpdate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panel2goUpdate.Controls.Add(this.button1);
            this.panel2goUpdate.Controls.Add(this.cmd2goInfo);
            this.panel2goUpdate.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2goUpdate.Location = new System.Drawing.Point(0, 145);
            this.panel2goUpdate.Name = "panel2goUpdate";
            this.panel2goUpdate.Size = new System.Drawing.Size(250, 83);
            this.panel2goUpdate.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.Gray;
            this.button1.Location = new System.Drawing.Point(0, 40);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(250, 40);
            this.button1.TabIndex = 1;
            this.button1.Text = "Einstellungen";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmd2goInfo
            // 
            this.cmd2goInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmd2goInfo.FlatAppearance.BorderSize = 0;
            this.cmd2goInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd2goInfo.ForeColor = System.Drawing.Color.Gray;
            this.cmd2goInfo.Location = new System.Drawing.Point(0, 0);
            this.cmd2goInfo.Name = "cmd2goInfo";
            this.cmd2goInfo.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.cmd2goInfo.Size = new System.Drawing.Size(250, 40);
            this.cmd2goInfo.TabIndex = 0;
            this.cmd2goInfo.Text = "Information";
            this.cmd2goInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmd2goInfo.UseVisualStyleBackColor = true;
            this.cmd2goInfo.Click += new System.EventHandler(this.cmd2goInfo_Click);
            // 
            // cmd2goUpdate
            // 
            this.cmd2goUpdate.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmd2goUpdate.FlatAppearance.BorderSize = 0;
            this.cmd2goUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd2goUpdate.ForeColor = System.Drawing.Color.Gainsboro;
            this.cmd2goUpdate.Location = new System.Drawing.Point(0, 100);
            this.cmd2goUpdate.Name = "cmd2goUpdate";
            this.cmd2goUpdate.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.cmd2goUpdate.Size = new System.Drawing.Size(250, 45);
            this.cmd2goUpdate.TabIndex = 0;
            this.cmd2goUpdate.Text = "HVO2go Update";
            this.cmd2goUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmd2goUpdate.UseVisualStyleBackColor = true;
            this.cmd2goUpdate.Click += new System.EventHandler(this.cmd2goUpdate_Click);
            // 
            // panelLogo
            // 
            this.panelLogo.Controls.Add(this.pictureBox1);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(250, 100);
            this.panelLogo.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(250, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.UpdateMain_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.UpdateMain_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.UpdateMain_MouseUp);
            // 
            // panelChildForm
            // 
            this.panelChildForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panelChildForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelChildForm.Location = new System.Drawing.Point(250, 0);
            this.panelChildForm.Name = "panelChildForm";
            this.panelChildForm.Size = new System.Drawing.Size(684, 664);
            this.panelChildForm.TabIndex = 1;
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(934, 664);
            this.Controls.Add(this.panelChildForm);
            this.Controls.Add(this.panelSideMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(600, 500);
            this.Name = "MainFrm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HVO-Updater";
            this.panelSideMenu.ResumeLayout(false);
            this.panelServoyInstall.ResumeLayout(false);
            this.panelPGUpdate.ResumeLayout(false);
            this.panelServoyUpdate.ResumeLayout(false);
            this.panel2goUpdate.ResumeLayout(false);
            this.panelLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Panel panel2goUpdate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button cmd2goInfo;
        private System.Windows.Forms.Button cmd2goUpdate;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Button cmdServoyUpdate;
        private System.Windows.Forms.Panel panelServoyInstall;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button cmdServoyInstall;
        private System.Windows.Forms.Panel panelPGUpdate;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button cmdPGinstall;
        private System.Windows.Forms.Panel panelServoyUpdate;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panelChildForm;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button cmdClose;
    }
}

